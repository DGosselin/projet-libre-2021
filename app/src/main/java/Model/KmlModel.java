package Model;

import org.osmdroid.bonuspack.kml.KmlDocument;

public class KmlModel {
    private KmlDocument kmlDocument;

    public KmlDocument getKmlDocument() {
        return kmlDocument;
    }

    public void setKmlDocument(KmlDocument kmlDocument) {
        this.kmlDocument = kmlDocument;
    }
}
