package Controller;

import android.app.DownloadManager;
import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import org.osmdroid.bonuspack.kml.KmlDocument;
import org.osmdroid.bonuspack.kml.KmlFeature;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;

public class KmlController {

    /*

    //////Depreciated function

    public KmlDocument loadKml(String url){
        KmlDocument kmlDocument = new KmlDocument();
        kmlDocument.parseKMLUrl(url);
        return kmlDocument;
    }*/

    /**
     * .
     * @param kmlDocument the kmlDocument to filter
     * @param key the type of filter to use
     * @param value the value with which to filter
     * @return a filtered KML document, depending on the key and value to look for given on entry
     */
    public KmlDocument filterDoc(KmlDocument kmlDocument, String key, String value){
        for(KmlFeature feature : kmlDocument.mKmlRoot.mItems){
            if(!feature.mExtendedData.get(key.toLowerCase()).equals(value)) feature.mVisibility=false;
            else feature.mVisibility=true;
        }
        return kmlDocument;
    }

    /**
     * @param kmlDocument the kmlDocument to unfilter
     * @return an unfiltered KML document
     */
    public KmlDocument cleanFilter(KmlDocument kmlDocument){

        for(KmlFeature feature : kmlDocument.mKmlRoot.mItems){
            feature.mVisibility=true;
        }

        return kmlDocument;
    }
    /*
    public boolean downloadJson(String urlPath,String downloadPath){

    }
     */


}
