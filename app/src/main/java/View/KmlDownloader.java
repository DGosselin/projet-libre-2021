package View;

import org.osmdroid.bonuspack.kml.KmlDocument;

import java.util.concurrent.Executor;

interface RepositoryCallback<T> {
    void onComplete(KmlDocument kmlDocument);
}

public class KmlDownloader {
    private final Executor executor;

    public void downloadKml(
            final String urlPath,
            final RepositoryCallback<KmlDocument> callback
    ) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    KmlDocument kmlDocument = new KmlDocument();
                    boolean isOk = kmlDocument.parseKMLUrl(urlPath);
                    callback.onComplete(kmlDocument);
                } catch (Exception e) {

                }
            }
        });
    }

    public KmlDownloader(Executor executor){
        this.executor = executor;
    }

}
