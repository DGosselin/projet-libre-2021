package View;

import Controller.GPSTracker;
import Controller.KmlController;
import Model.KmlModel;
import android.Manifest;
import android.app.Activity;
import android.content.*;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import com.example.myapplication.R;
import org.osmdroid.api.IMapController;
import org.osmdroid.bonuspack.kml.KmlDocument;
import org.osmdroid.bonuspack.kml.KmlFeature;
import org.osmdroid.config.Configuration;
import org.osmdroid.tileprovider.tilesource.OnlineTileSourceBase;
import org.osmdroid.tileprovider.tilesource.XYTileSource;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.FolderOverlay;
import org.osmdroid.views.overlay.mylocation.GpsMyLocationProvider;
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay;

import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MainActivity extends Activity {
    MapView map = null;
    KmlModel model = new KmlModel();
    KmlController controller = new KmlController();
    private int kmlOverlayIndex;
    private int gpsOverlayIndex;
    GPSTracker gps;

    @Override public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.onCreate(savedInstanceState);

        ExecutorService executorService = Executors.newFixedThreadPool(1);

        //handle permissions first, before map is created. not depicted here
        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_WIFI_STATE) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_NETWORK_STATE) != PackageManager.PERMISSION_GRANTED)  {

            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.INTERNET, Manifest.permission.ACCESS_WIFI_STATE, Manifest.permission.ACCESS_NETWORK_STATE},
                    1);
        }

        //load/initialize the osmdroid configuration, this can be done
        Context ctx = getApplicationContext();
        Configuration.getInstance().load(ctx, PreferenceManager.getDefaultSharedPreferences(ctx));
        //setting this before the layout is inflated is a good idea
        //it 'should' ensure that the map has a writable location for the map cache, even without permissions
        //if no tiles are displayed, you can try overriding the cache path using Configuration.getInstance().setCachePath
        //see also StorageUtils
        //note, the load method also sets the HTTP User Agent to your application's package name, abusing osm's tile servers will get you banned based on this string

        //inflate and create the map
        setContentView(R.layout.main);

        map = (MapView) findViewById(R.id.map);
        map.setBuiltInZoomControls(true);
        map.setMultiTouchControls(true);
        final IMapController mapController = map.getController();
        mapController.setZoom(9.5);

        // On initialise ces valeurs avec les coordonnées de la ville de Tours
        double latitude = 47.383333;
        double longitude = 0.683333;

        GeoPoint startPoint = new GeoPoint(47.383333, 0.683333);

        mapController.setCenter(startPoint);
        OnlineTileSourceBase tileSource = new XYTileSource("Mapnik", 1, 18,
                256, ".png", new String[]{
                "https://a.tile.openstreetmap.org/",
                "https://b.tile.openstreetmap.org/",
                "https://c.tile.openstreetmap.org/"}, "© OpenStreetMap contributors");

        map.setTileSource(tileSource);


        final KmlDownloader kmlDownloader = new KmlDownloader(executorService);

        kmlDownloader.downloadKml("https://data.tours-metropole.fr/explore/dataset/aides-etudiants-tours-metropole-val-de-loire/download/?format=kml&timezone=Europe/Berlin&lang=fr", new RepositoryCallback<KmlDocument>() {
            @Override
            public void onComplete(KmlDocument kmlDocument) {
                model.setKmlDocument(kmlDocument);
                FolderOverlay kmlOverlay = (FolderOverlay) model.getKmlDocument().mKmlRoot.buildOverlay(map, null, null, model.getKmlDocument());
                map.getOverlays().add(kmlOverlay);
                kmlOverlayIndex = map.getOverlays().size()-1;
                map.invalidate();
            }
        });

        // Add gps location
        GpsMyLocationProvider prov= new GpsMyLocationProvider(getApplicationContext());
        prov.addLocationSource(LocationManager.NETWORK_PROVIDER);
        MyLocationNewOverlay locationOverlay = new MyLocationNewOverlay(prov, map);
        locationOverlay.enableMyLocation();
        map.getOverlays().add(locationOverlay);
        gpsOverlayIndex = map.getOverlays().size()-1;

        final Spinner keySpinner = (Spinner) findViewById(R.id.keySpinner);


// Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.filter_array, android.R.layout.simple_spinner_item);
// Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
        keySpinner.setAdapter(adapter);

        final Spinner valueSpinner = (Spinner) findViewById(R.id.valueSpinner);
        valueSpinner.setEnabled(false);

        keySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(keySpinner.getItemAtPosition(position).equals("")) valueSpinner.setEnabled(false);

                else {
                    populateValueSpinner(valueSpinner,model.getKmlDocument(),keySpinner.getSelectedItem().toString());
                    valueSpinner.setEnabled(true);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                valueSpinner.setEnabled(false);
            }

        });

        valueSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                FolderOverlay kmlOverlay;

                if(valueSpinner.getSelectedItem().equals("")){
                    model.setKmlDocument(controller.cleanFilter(model.getKmlDocument()));
                }

                else {
                    model.setKmlDocument(controller.filterDoc(model.getKmlDocument(),keySpinner.getSelectedItem().toString().toLowerCase(),valueSpinner.getSelectedItem().toString()));
                }

                kmlOverlay = (FolderOverlay) model.getKmlDocument().mKmlRoot.buildOverlay(map, null, null, model.getKmlDocument());
                map.getOverlays().set(kmlOverlayIndex,kmlOverlay);
                map.invalidate();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {

            }

        });

    }

    public void onResume(){
        super.onResume();
        //this will refresh the osmdroid configuration on resuming.
        //if you make changes to the configuration, use
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        Configuration.getInstance().load(this, PreferenceManager.getDefaultSharedPreferences(this));
        map.onResume(); //needed for compass, my location overlays, v6.0.0 and up
    }

    public void onPause(){
        super.onPause();
        //this will refresh the osmdroid configuration on resuming.
        //if you make changes to the configuration, use
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        Configuration.getInstance().save(this, prefs);
        map.onPause();  //needed for compass, my location overlays, v6.0.0 and up
    }

    private void populateValueSpinner(Spinner spinner, KmlDocument kmlDocument, String key){
        ArrayList<String> values = new ArrayList<String>();
        boolean match = false;

        for(KmlFeature feature : kmlDocument.mKmlRoot.mItems){
            if(values.size()>0){
                for(String value : values){
                    if(feature.mExtendedData.get(key.toLowerCase()).equals(value)) {
                        match = true;
                        break;
                    }
                }
                if(!match) values.add(feature.mExtendedData.get(key.toLowerCase()));
                match = false;
            }
            else values.add("");
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                        this, android.R.layout.simple_spinner_item, values);
// Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
        spinner.setAdapter(adapter);
    }

}
